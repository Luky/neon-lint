<?php declare(strict_types = 1);

$console = new \Symfony\Component\Console\Application('NEON Linter');

$console->setDefaultCommand('run');

$console->add(new \Luky\NeonLint\LintCommand());

exit($console->run());
