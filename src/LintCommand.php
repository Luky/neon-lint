<?php declare(strict_types = 1);

namespace Luky\NeonLint;

use Nette\Neon\Decoder;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

final class LintCommand extends Command
{
    private const ARG_PATHS = 'paths';


    protected function configure(): void
    {
        $this->addArgument(self::ARG_PATHS, InputArgument::IS_ARRAY);
    }


    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $paths = $input->getArgument(self::ARG_PATHS);


        $finder = \Nette\Utils\Finder::findFiles('*.neon')->from(__DIR__ . '/../')->exclude('vendor');

        $decoder = new Decoder();
    }


    private function processFile(\SplFileInfo $file): bool
    {
        
    }
}
